//
//  Celula.swift
//  Aula_23062022
//
//  Created by COTEMIG on 23/06/22.
//

import UIKit

class Celula: UITableViewCell {

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var titulo: UILabel!
    
    @IBOutlet weak var imagem: UIImageView!
}
