//
//  ViewController.swift
//  Aula_23062022
//
//  Created by COTEMIG on 23/06/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource{
    
    var listaDeValores = [String]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeValores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabela.dequeueReusableCell(withIdentifier: "MinhaChave", for: indexPath) as? Celula
        
        cell?.titulo.text = listaDeValores[indexPath.row]
        
        
        if indexPath.row % 3 == 0 {
            cell?.imagem.image = UIImage(systemName: "house")
            cell?.container.backgroundColor = .red
        } else {
            cell?.container.backgroundColor = .yellow
        }
        
        return cell ?? UITableViewCell()
    }
    

    @IBOutlet weak var tabela: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        listaDeValores.append("João")
        listaDeValores.append("Marcos")
        listaDeValores.append("Ana")
        listaDeValores.append("Beatriz")
        
        tabela.dataSource = self
        tabela.delegate = self
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        performSegue(withIdentifier: "detalhes", sender: nil)
    }
}

